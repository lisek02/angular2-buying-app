angular2 seed
========

Seed for application in angular2 with typescript, webpack, jade and sass support

## Usage

Install deps
```
npm install
```

Build
```
npm run build
```

Start
```
npm run start
```