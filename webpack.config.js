const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ProvidePlugin = require('webpack/lib/ProvidePlugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const autoprefixer = require('autoprefixer');

module.exports = {
  entry: {
    polyfills: "./src/polyfills",
    vendor: "./src/vendor",
    app: "./src/index"
  },
  output: {
    path: __dirname,
    filename: "./dist/[name].bundle.js"
  },
  resolve: {
    extensions: ['', '.js', '.ts']
  },
  devtool: 'source-map',
  module: {
    loaders: [
      {
        test: /\.ts/,
        loader: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.jade/,
        loader: 'raw!jade-html',
        exclude: /node_modules/
      },
      {
        test: /^(?!.*component).*\.css$/,
        loader: ExtractTextPlugin.extract('style', 'css?sourceMap!resolve-url')
      },
      {
        test: /\.component\.css$/,
        loader: 'raw'
      },
      {
        test: /^(?!.*component).*\.scss$/,
        loader: ExtractTextPlugin.extract('style', 'css?sourceMap!postcss!resolve-url!sass?sourceMap')
      },
      {
        test: /\.component\.scss$/,
        loader: 'raw!postcss!resolve-url!sass'
      },
      { test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,   loader: "url?limit=10000&mimetype=application/font-woff&name=./dist/[name].[ext]?[hash]" },
      { test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,  loader: "url?limit=10000&mimetype=application/font-woff2&name=./dist/[name].[ext]?[hash]" },
      { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=application/octet-stream&name=./dist/[name].[ext]?[hash]" },
      { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file?name=./dist/[name].[ext]?[hash]" },
      { test: /\.(png|jpg)$/,                  loader: "file?name=./dist/[name].[ext]?[hash]" },
      { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=image/svg+xml&name=./dist/[name].[ext]?[hash]" }
    ],
    noParse: [ /zone\.js\/dist\/.+/, /angular2\/bundles\/.+/ ]
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin(
      {
        name: "polyfills",
        filename: "./dist/polyfills.bundle.js"
      },
      {
        name: "vendor",
        filename: "./dist/vendor.bundle.js"
      }
    ),
    new HtmlWebpackPlugin({
      template: './src/index.jade',
      filename: 'index.html',
      minify: false
    }),
    new ProvidePlugin({
      jQuery: 'jquery',
      $: 'jquery',
      jquery: 'jquery'
    }),
    new ExtractTextPlugin('./dist/[name].css')
  ],
  postcss: function () {
    return [autoprefixer];
  },
};
