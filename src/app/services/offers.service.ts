import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

@Injectable()
export class OffersService {
  constructor(private http: Http) {}

  public getList(): any {
    return this.http.request('http://localhost:3000/api/offers')
      .map(response => response.json());
  }
}