import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import { User } from '../models/user';
import { SignIn } from '../models/signin';
import { Headers, Http, Response } from '@angular/http';

@Injectable()
export class SessionService {
  constructor(private http: Http) {
    this.options = new Headers({
      'Content-Type': 'application/json'
    });
  }

  private options;

  public signUp(user: User): Observable<Response> {
    return this.http.post('http://localhost:3000/api/users', user, this.options)
  }
}