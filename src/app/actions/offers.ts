export const ADD_OFFER = 'ADD_OFFER';
export const REMOVE_OFFER = 'REMOVE_OFFER';
export const UPDATE_OFFER = 'UPDATE_OFFER';
export const SET_OFFERS = 'SET_OFFERS';