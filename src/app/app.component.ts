import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { FooterComponent } from './components/footer/footer.component';

@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'my-app',
  directives: [
    HeaderComponent,
    FooterComponent
  ],
  template: require('./app.component.jade'),
  styles: [ require('./app.component.scss') ]
})

export class AppComponent {}
