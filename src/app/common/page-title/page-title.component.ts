import { Component } from '@angular/core';

@Component({
    selector: 'page-title',
    inputs: ['title'],
    template: require('./page-title.component.jade'),
    styles: [ require('./page-title.component.scss') ]
})
export class PageTitleComponent {
    title: String;
}