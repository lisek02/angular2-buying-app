import { Offer } from '../models/offer';
import { OffersState } from '../reducers/offers';

export interface AppState {
    offers: Offer[]
}