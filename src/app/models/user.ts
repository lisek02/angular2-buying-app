export interface User {
  _id: String,
  email: String,
  phoneNumber: String,
  password: String,
  passwordConfirmation: String,
  token: String;
  admin: boolean
}