export interface Offer {
  _id: string;
  aptNumber: number,
  city: string,
  houseNumber: number,
  street: string,
  description: string;
  area: number,
  balcony: boolean,
  floor: number,
  rooms: number,
  picture: string;
}