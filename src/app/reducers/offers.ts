import { ActionReducer, Action } from '@ngrx/store';
import { Observable } from 'rxjs/Observable';
import { Offer } from '../models/offer';
import { AppState } from '../common/app-state.interface';
import {
  ADD_OFFER,
  REMOVE_OFFER,
  UPDATE_OFFER,
  SET_OFFERS
} from '../actions/offers';

export interface OffersState {
  offers: Offer[];
}

export const offers = (state = [], action: Action) => {
  switch (action.type) {
    case ADD_OFFER:
      return [
        ...state,
        Object.assign({}, {
          _id: action.payload._id,
          aptNumber: action.payload.aptNumber,
          city: action.payload.city,
          houseNumber: action.payload.houseNumber,
          street: action.payload.street,
          description: action.payload.description,
          area: action.payload.area,
          balcony: action.payload.balcony,
          floor: action.payload.floor,
          rooms: action.payload.rooms,
          picture: action.payload.picture
        })
      ]
    case REMOVE_OFFER:
      return state
    case UPDATE_OFFER:
      return state
        .map(offer => {
          if(offer._id !== action.payload._id) {
            return offer;
          }
          return Object.assign({}, offer, {
            _id: action.payload._id,
            aptNumber: action.payload.aptNumber,
            city: action.payload.city,
            houseNumber: action.payload.houseNumber,
            street: action.payload.street,
            description: action.payload.description,
            area: action.payload.area,
            balcony: action.payload.balcony,
            floor: action.payload.floor,
            rooms: action.payload.rooms,
            picture: action.payload.picture
          })
        });
    case SET_OFFERS:
      return action.payload
    default:
      return state;
    }
}

export function getOffer(id: string) {
  return (state: Observable<OffersState>) => {
    return state
      .flatMap(entities => entities.offers)
      .filter(offer => offer._id === id)
  }
}
