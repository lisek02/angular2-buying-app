import { Component } from '@angular/core';

@Component({
    selector: 'last-watched',
    template: require('./last-watched.component.jade'),
    styles: [ require('./last-watched.component.scss') ]
})
export class LastWatchedComponent { }