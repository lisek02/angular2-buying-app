import { Component } from '@angular/core';

@Component({
    selector: 'search-box',
    template: require('./search-box.component.jade'),
    styles: [ require('./search-box.component.scss') ]
})
export class SearchBoxComponent { }