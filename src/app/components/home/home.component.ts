import { Component } from '@angular/core';
import { SearchBoxComponent } from './search-box/search-box.component';
import { LastWatchedComponent } from './last-watched/last-watched.component';

@Component({
    selector: 'home',
    directives: [
        SearchBoxComponent,
        LastWatchedComponent
    ],
    template: require('./home.component.jade'),
    styles: [ require('./home.component.scss') ]
})
export class HomeComponent { }