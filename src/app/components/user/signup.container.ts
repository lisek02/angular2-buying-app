import { Component } from '@angular/core';
import { User } from '../../models/user';
import { Router } from '@ngrx/router';
import { PageTitleComponent } from '../../common/page-title/page-title.component';
import { SignupFormComponent } from './signup/signup-form.component';
import { SessionService } from '../../services/session.service';

@Component({
  selector: 'signup-form-container',
  directives: [ 
    PageTitleComponent,
    SignupFormComponent
  ],
  providers: [ SessionService ],
  template: `
    <page-title [title]='title'></page-title>
    <div class='container'>
      <signup-form (signUp)='signUp($event)'></signup-form>
    </div>
  `
})
export class SignupFormContainer {
  private title: String;
  private user: User;

  constructor(
    private router: Router,
    private sessionService: SessionService
    ) {
    this.title = 'Zarejestruj się';
  }

  signUp(user: User) {
    if (user.password === user.passwordConfirmation)
      this.sessionService.signUp(user)
        .subscribe(response => {
          // this.router.go('/user/signin');
        })
  }
}