import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import {
  FORM_DIRECTIVES,
  REACTIVE_FORM_DIRECTIVES,
  FormGroup,
  FormBuilder,
  FormControl,
  Validators
} from '@angular/forms';

@Component({
  selector: 'signup-form',
  directives: [
    FORM_DIRECTIVES,
    REACTIVE_FORM_DIRECTIVES,
  ],
  template: require('./signup-form.component.jade'),
  styles: [ require('./signup-form.component.scss') ]
})

export class SignupFormComponent {
  private signupForm: FormGroup;
  private formSubmitted: boolean;

  private email: FormControl;
  private phoneNumber: FormControl;
  private password: FormControl;
  private passwordConfirmation: FormControl;

  @Output() signUp : EventEmitter<Event> = new EventEmitter<Event>();

  constructor(formBuilder: FormBuilder) {
    this.formSubmitted = false;

    this.email = new FormControl('', Validators.required);
    this.phoneNumber = new FormControl('', Validators.required);
    this.password = new FormControl('', Validators.required);
    this.passwordConfirmation = new FormControl('', Validators.required);

    this.signupForm = formBuilder.group({
      email: this.email,
      phoneNumber: this.phoneNumber,
      password: this.password,
      passwordConfirmation: this.passwordConfirmation
    });

  }

  onSubmit() {
    this.formSubmitted = true;
    if (this.signupForm.valid) {
      this.signUp.emit(this.signupForm.value);
    }
  }
}