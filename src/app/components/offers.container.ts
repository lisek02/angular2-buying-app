import { Component } from '@angular/core';

@Component({
  selector: 'offers',
  template: `
    <route-view></route-view>
  `
})
export class OffersContainer {}