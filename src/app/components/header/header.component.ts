import { Component } from '@angular/core';

@Component({
    selector: 'header',
    template: require('./header.component.jade'),
    styles: [ require('./header.component.scss') ]
})
export class HeaderComponent { }