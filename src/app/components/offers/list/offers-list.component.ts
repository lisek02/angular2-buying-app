import { Component, Input } from '@angular/core';
import { Offer } from '../../../models/offer';
import { PageTitleComponent } from '../../../common/page-title/page-title.component';

@Component({
  selector: 'offers-list',
  directives: [ PageTitleComponent ],
  template: require('./offers-list.component.jade'),
  styles: [ require('./offers-list.component.scss') ]
})
export class OffersListComponent {
  @Input() offers: Offer[]
}