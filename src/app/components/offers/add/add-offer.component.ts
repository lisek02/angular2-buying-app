import {
  Component,
  Output,
  EventEmitter
} from '@angular/core';
import { OffersFormComponent } from '../form/offers-form.component';

@Component({
  selector: 'add-offer',
  directives: [
    OffersFormComponent
  ],
  template: require('./add-offer.component.jade'),
  styles: [ require('./add-offer.component.scss') ]
})
export class AddOfferComponent {
  @Output() addOffer: EventEmitter<Event> = new EventEmitter<Event>();

  constructor() {}

  passOffer(offer) {
    this.addOffer.emit(offer);
  }
}
