import { Component } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { OffersState } from '../../reducers/offers';
import { OffersListComponent } from './list/offers-list.component';
import { Offer } from '../../models/offer';
import { SET_OFFERS } from '../../actions/offers';
import { OffersService } from '../../services/offers.service';

@Component({
  selector: 'offers-list-container',
  directives: [ OffersListComponent ],
  providers: [ OffersService ],
  template: `
    <offers-list [offers]='offers | async'></offers-list>
  `
})

export class OffersListContainer {
  private offers: Observable<Offer[]>;

  constructor(
    private store: Store<OffersState>,
    private offersService: OffersService
    ) {
      this.offersService.getList()
        .subscribe(offers => this.store.dispatch({ type: SET_OFFERS, payload: offers }));
      this.offers = store.select<Offer[]>('offers');
    }
}