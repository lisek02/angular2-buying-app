import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Offer } from '../../../models/offer';
import { OffersFormComponent } from '../form/offers-form.component';

@Component({
  selector: 'edit-offer',
  directives: [
    OffersFormComponent
  ],
  template: require('./edit-offer.component.jade'),
  styles: [ require('./edit-offer.component.scss') ]
})
export class EditOfferComponent {
  @Input() offer: Observable<Offer>;
  @Output() editOffer: EventEmitter<Event> = new EventEmitter<Event>();

  constructor() { }

  passOffer(offer) {
    this.editOffer.emit(offer);
  }
}
