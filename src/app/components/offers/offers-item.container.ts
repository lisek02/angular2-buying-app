import { Component } from '@angular/core';
import { RouteParams } from '@ngrx/router';
import { Observable } from 'rxjs/Observable';
import { Store } from '@ngrx/store';
import { AppState } from '../../common/app-state.interface';
import { Offer } from '../../models/offer';
import { OffersItemComponent } from './item/offers-item.component';
import { getOffer } from '../../reducers/offers';
import 'rxjs/add/operator/switchMap';
import '@ngrx/core/add/operator/select';

@Component({
  selector: 'offers-item-container',
  directives: [ OffersItemComponent ],
  template: `
    <offers-item [offer]='offer | async'></offers-item>
  `
})
export class OffersItemContainer {
  private offer: Observable<Offer>;

  constructor(routeParams: RouteParams, private store: Store<AppState>) {
    this.offer = routeParams
      .select<string>('id')
      .switchMap(id => store.let(getOffer(id)))
  }
}