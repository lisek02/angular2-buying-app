import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit } from '@angular/core';
import {
  FORM_DIRECTIVES,
  REACTIVE_FORM_DIRECTIVES,
  AbstractControl,
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { Offer } from '../../../models/offer';
import { PageTitleComponent } from '../../../common/page-title/page-title.component';
import 'rxjs/add/operator/pluck';

@Component({
  selector: 'offers-form',
  directives: [
    FORM_DIRECTIVES,
    REACTIVE_FORM_DIRECTIVES,
    PageTitleComponent
  ],
  template: require('./offers-form.component.jade'),
  styles: [ require('./offers-form.component.scss') ]
})

export class OffersFormComponent implements OnInit {
  private offersForm: FormGroup;
  private formSubmitted: boolean;

  private id: String;
  private city: FormControl;
  private street: FormControl;
  private houseNumber: FormControl;

  @Output() passOffer : EventEmitter<Event> = new EventEmitter<Event>();
  @Input() offer: Offer;

  constructor(formBuilder: FormBuilder) {
    this.formSubmitted = false;
    this.id = '';
    this.city = new FormControl('', Validators.required);
    this.street = new FormControl('', Validators.required);
    this.houseNumber = new FormControl('', Validators.required);

    this.offersForm = formBuilder.group({
      city: this.city,
      street: this.street,
      houseNumber: this.houseNumber
    })
  }

  ngOnInit() {
    if (!!this.offer) {
      this.id = this.offer.id;
      this.city.updateValue(this.offer.city || '');
      this.street.updateValue(this.offer.street || '');
      this.houseNumber.updateValue(this.offer.houseNumber || '');
    }
  }

  onSubmit() {
    this.formSubmitted = true;
    if (this.offersForm.valid) {
      this.offersForm.value.id = this.id;
      this.passOffer.emit(this.offersForm.value);
    }
  }
}