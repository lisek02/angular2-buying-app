import { Component, Input } from '@angular/core';
import { Offer } from '../../../models/offer';
import { PageTitleComponent } from '../../../common/page-title/page-title.component';

@Component({
  selector: 'offers-item',
  directives: [
    PageTitleComponent
  ],
  template: require('./offers-item.component.jade'),
  styles: [ require('./offers-item.component.scss') ]
})
export class OffersItemComponent {
  @Input() offer: Offer;

  constructor() { }
}