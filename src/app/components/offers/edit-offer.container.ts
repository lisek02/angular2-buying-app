import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router, RouteParams } from '@ngrx/router';
import { Observable } from 'rxjs/Observable';
import { AppState } from '../../common/app-state.interface';
import { UPDATE_OFFER } from '../../actions/offers';
import { Offer } from '../../models/offer';
import { getOffer } from '../../reducers/offers';
import { EditOfferComponent } from './edit/edit-offer.component';
import { PageTitleComponent } from '../../common/page-title/page-title.component';

@Component({
  selector: 'add-offer-container',
  directives: [
    EditOfferComponent,
    PageTitleComponent
  ],
  template: `
    <page-title [title]='title'></page-title>
    <div class='container'>
      <edit-offer [offer]='offer' (editOffer)='editOffer($event)'></edit-offer>
    </div>
  `
})
export class EditOfferContainer {
  private title: String;
  private offer: Observable<Offer>;

  constructor(private router: Router, routeParams: RouteParams, private store: Store<AppState>) {
    this.title = 'Edytuj ogłoszenie';
    this.offer = routeParams
      .select<string>('id')
      .switchMap(id => store.let(getOffer(id)))
  }

  editOffer(offer) {
    this.store.dispatch({ type: UPDATE_OFFER, payload: offer })
    this.router.go('/offers');
  }
}