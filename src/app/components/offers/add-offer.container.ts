import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Router } from '@ngrx/router';
import { AppState } from '../../common/app-state.interface';
import { ADD_OFFER } from '../../actions/offers';
import { AddOfferComponent } from './add/add-offer.component';
import { PageTitleComponent } from '../../common/page-title/page-title.component';

@Component({
  selector: 'add-offer-container',
  directives: [
    AddOfferComponent,
    PageTitleComponent
  ],
  template: `
    <page-title [title]='title'></page-title>
    <div class='container'>
      <add-offer (addOffer)='addOffer($event)'></add-offer>
    </div>
  `
})
export class AddOfferContainer {
  private title: String;

  constructor(private router: Router, private store: Store<AppState>) {
    this.title = 'Dodaj ogłoszenie';
  }

  addOffer(offer) {
    this.store.dispatch({ type: ADD_OFFER, payload: offer })
    this.router.go('/offers');
  }
}