import { Component } from '@angular/core';

@Component({
  selector: 'user',
  template: `
    <route-view></route-view>
  `
})
export class UserContainer {}