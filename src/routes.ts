import { Routes } from '@ngrx/router';
import { HomeComponent } from './app/components/home/home.component';
import { OffersContainer } from './app/components/offers.container';
import { OffersListContainer } from './app/components/offers/offers-list.container';
import { OffersItemContainer } from './app/components/offers/offers-item.container';
import { AddOfferContainer } from './app/components/offers/add-offer.container';
import { EditOfferContainer } from './app/components/offers/edit-offer.container';
import { UserContainer } from './app/components/user.container.ts';
import { SignupFormContainer } from './app/components/user/signup.container';
import { SigninFormContainer } from './app/components/user/signin.container';

export const routes: Routes = [
  {
    path: '/',
    component: HomeComponent
  },
  {
    path: '/offers',
    component: OffersContainer,
    index: {
      component: OffersListContainer
    },
    children: [
      {
        path: '/add',
        component: AddOfferContainer
      },
      {
        path: '/:id',
        component: OffersItemContainer
      },
      {
        path: '/:id/edit',
        component: EditOfferContainer
      }
    ]
  },
  {
    path: '/user',
    component: UserContainer,
    index: {
      component: SignupFormContainer
    },
    children: [
      {
        path: '/signin',
        component: SigninFormContainer
      }
    ]
  }
]